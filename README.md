# Heat Transfer Calculation - effectiveness-NTU method

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/5dade9b0921944c4982e6f4cd860e02d)](https://www.codacy.com/manual/forray.zsolt/ntu-method?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Zsolt-Forray/ntu-method&amp;utm_campaign=Badge_Grade)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-370/)

## Description
The effectiveness - NTU method is used to calculate the rate of heat transfer when outlet temperatures are not
available.

## Usage

![Screenshot](/png/fig.png)

### Usage Example

```python
#!/usr/bin/python3

import ntu_method as nm

nm.run_app()
```

**Initial Parameters:**

* Flow tye (counter / parallel)
* Air inlet temperature (°C)
* Air flow rate (kg/s)
* Air specific heat (J/kg°C)
* Water inlet temperature (°C)
* Water flow rate (kg/s)
* Water specific heat (J/kg°C)
* Heat exchanger surface (m2)
* Overall heat transfer coefficient (W/m2°C)

## LICENSE
MIT

## Contributions
Contributions to this repository are always welcome.
This repo is maintained by Zsolt Forray (forray.zsolt@gmail.com).
