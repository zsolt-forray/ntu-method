#!/usr/bin/python3


"""
Chart plotting
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '24/11/2019'
__status__  = 'Development'


import os
import matplotlib.image as img
from matplotlib import pyplot as plt
from chart_settings import ChartSettings


class ChartPlotting(ChartSettings):
    def __init__(self):
        ChartSettings.__init__(self)

    def run_plot(self):
        self.set_chart()
        self.T1sl.on_changed(self.update_temp)
        self.T2sl.on_changed(self.update_temp)
        self.mw.on_changed(self.update_mass_flow)
        self.mc.on_changed(self.update_mass_flow)
        self.radio.on_clicked(self.update_flow_type)
        self.button.on_clicked(ChartPlotting.close_chart)
        plt.show()

    def update_temp(self, val):
        self.T_warm_in = int(self.T1sl.val)
        self.T_cold_in = int(self.T2sl.val)

        T_warm_out, T_cold_out, e, q_kw = self.run_calc()

        self.t1in.set_text("T1_in = {:.0f}°C".format(self.T_warm_in))
        self.t2in.set_text("T2_in = {:.0f}°C".format(self.T_cold_in))
        self.t2in2.set_text("T2_in = {:.0f}°C".format(self.T_cold_in))
        self.t1out.set_text("T1_out = {:.0f}°C".format(T_warm_out))
        self.t2out.set_text("T2_out = {:.0f}°C".format(T_cold_out))
        self.t2out2.set_text("T2_out = {:.0f}°C".format(T_cold_out))

        self.tbox.set_text(self.res_text.format(q_kw, e))
        self.fig.canvas.draw_idle()

    def update_mass_flow(self, val):
        self.m_warm = round(self.mw.val,1)
        self.m_cold = round(self.mc.val,1)

        T_warm_out, T_cold_out, e, q_kw = self.run_calc()

        self.t1out.set_text("T1_out = {:.0f}°C".format(T_warm_out))
        self.t2out.set_text("T2_out = {:.0f}°C".format(T_cold_out))
        self.t2out2.set_text("T2_out = {:.0f}°C".format(T_cold_out))

        self.tbox.set_text(self.res_text.format(q_kw, e))
        self.fig.canvas.draw_idle()

    def update_flow_type(self, label):
        self.flow_type = label

        T_warm_out, T_cold_out, e, q_kw = self.run_calc()

        if self.flow_type == "parallel":
            # parallel flow
            self.pt1i = (0.11, 4.9)
            self.pt1o = (1.34, 4.9)
            self.pt2i = (0.11, 4.5)
            self.pt2i2 = (0.11, 5.3)
            self.pt2o = (1.34, 4.5)
            self.pt2o2 = (1.34, 5.3)

            self.t2in2.set_visible(True)
            self.t2out2.set_visible(True)

            self.t1in.set_position((self.pt1i))
            self.t1out.set_position((self.pt1o))
            self.t2in.set_position((self.pt2i))
            self.t2in2.set_position((self.pt2i2))
            self.t2out.set_position((self.pt2o))
            self.t2out2.set_position((self.pt2o2))

            self.t2in2.set_text("T2_in = {:.0f}°C".format(self.T_cold_in))
            self.t1out.set_text("T1_out = {:.0f}°C".format(T_warm_out))
            self.t2out.set_text("T2_out = {:.0f}°C".format(T_cold_out))
            self.t2out2.set_text("T2_out = {:.0f}°C".format(T_cold_out))

            self.t2out2.set_backgroundcolor("y")

        elif self.flow_type == "counter":
            # counter flow
            self.pt1i = (0.6, 3.9)
            self.pt1o = (1.2, 6.1)
            self.pt2i = (1.34, 4.9)
            self.pt2o = (0.11, 4.9)

            self.t1in.set_position((self.pt1i))
            self.t1out.set_position((self.pt1o))
            self.t2in.set_position((self.pt2i))
            self.t2out.set_position((self.pt2o))

            self.t1out.set_text("T1_out = {:.0f}°C".format(T_warm_out))
            self.t2out.set_text("T2_out = {:.0f}°C".format(T_cold_out))

            self.t2in2.set_visible(False)
            self.t2out2.set_visible(False)

        self.hex_config_plot = img.imread(os.path.join(self.png_loc, self.hexpng))
        self.imagebox.set_data(self.hex_config_plot)

        self.tbox.set_text(self.res_text.format(q_kw, e))
        self.fig.canvas.draw_idle()

    @staticmethod
    def close_chart(evt):
        plt.close()
