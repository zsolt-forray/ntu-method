#!/usr/bin/python3


"""
Chart settings
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '24/11/2019'
__status__  = 'Development'


import os
import json
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import matplotlib.image as img
from matplotlib.offsetbox import AnnotationBbox, OffsetImage


class ChartSettings:
    def __init__(self):
        # HEX arrangement .png location
        self.png_loc = os.path.join(os.path.dirname(os.path.abspath(__file__)), "png")
        # JSON file location
        self.json_loc = os.path.join(os.path.dirname(os.path.abspath(__file__)), "json")
        # Label axes position
        self.text_axes_pos = (0.055, 0.15, 0.5, 0.1)

    @staticmethod
    def set_axes(*pos):
        axes = plt.axes([*pos])
        return axes

    @staticmethod
    def set_sliders_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return Slider(ChartSettings.set_axes(*positions), params["text"],\
                                    params["min"], params["max"],\
                                    params["init"], params["valfmt"],\
                                    color=params["color"])

    @staticmethod
    def set_radio_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return RadioButtons(ChartSettings.set_axes(*positions), params["text"], params["active"])

    @staticmethod
    def set_buttons_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return Button(ChartSettings.set_axes(*positions), params["text"],\
                                    color=params["color"], hovercolor=params["hovercolor"])

    def set_labels_params(self, params, text):
        positions = params["x"], params["y"]
        return self.ax_text.text(*positions, text, size=params["size"],\
                                 style=params["style"], color=params["color"],\
                                 backgroundcolor=params["bgcolor"])

    def set_sliders(self):
        # Sliders
        with open(os.path.join(self.json_loc, "sliders_parameters.json")) as json_file:
            slider_params = json.load(json_file)

        # Sliders for warm inlet temperature
        T1sl_params = slider_params["T1sl"]
        self.T1sl = ChartSettings.set_sliders_params(T1sl_params)

        # Sliders for cold inlet temperature
        T2sl_params = slider_params["T2sl"]
        self.T2sl = ChartSettings.set_sliders_params(T2sl_params)

        # Sliders for warm air mass flow rate
        mw_params = slider_params["mw"]
        self.mw = ChartSettings.set_sliders_params(mw_params)

        # Sliders for cold water mass flow rate
        mc_params = slider_params["mc"]
        self.mc = ChartSettings.set_sliders_params(mc_params)

    def set_radios(self):
        # RadioButtons
        with open(os.path.join(self.json_loc, "radio_parameters.json")) as json_file:
            radio_params = json.load(json_file)

        # RadioButtons for flow type
        flow_type_params = radio_params["flow_type"]
        self.radio = ChartSettings.set_radio_params(flow_type_params)

    def set_buttons(self):
        # Buttons
        with open(os.path.join(self.json_loc, "button_parameters.json")) as json_file:
            button_params = json.load(json_file)

        # Button for close
        close_button_params = button_params["close"]
        self.button = ChartSettings.set_buttons_params(close_button_params)

    def set_labels(self):
        self.ax_text = ChartSettings.set_axes(*self.text_axes_pos)
        self.ax_text.axis("off")

        # Labels
        with open(os.path.join(self.json_loc, "label_parameters.json")) as json_file:
            label_params = json.load(json_file)

        t1in_params = label_params["t1in"]
        t1in_text = "T1_in = {:.0f}°C".format(self.T_warm_in)
        self.t1in = self.set_labels_params(t1in_params, t1in_text)

        t1out_params = label_params["t1out"]
        t1out_text = "T1_out = {:.0f}°C".format(self.T_warm_out)
        self.t1out = self.set_labels_params(t1out_params, t1out_text)

        t2in_params = label_params["t2in"]
        t2in_text = "T2_in = {:.0f}°C".format(self.T_cold_in)
        self.t2in = self.set_labels_params(t2in_params, t2in_text)

        t2out_params = label_params["t2out"]
        t2out_text = "T2_out = {:.0f}°C".format(self.T_cold_out)
        self.t2out = self.set_labels_params(t2out_params, t2out_text)

        t2in2_params = label_params["t2in2"]
        self.t2in2 = self.set_labels_params(t2in2_params, "")

        t2out2_params = label_params["t2out2"]
        self.t2out2 = self.set_labels_params(t2out2_params, "")

        c1_params = label_params["c1"]
        c1_text = "ca = {:.0f} J/kg°C".format(self.c_warm)
        self.set_labels_params(c1_params, c1_text)

        c2_params = label_params["c2"]
        c2_text = "cw = {:.0f} J/kg°C".format(self.c_cold)
        self.set_labels_params(c2_params, c2_text)

        A_params = label_params["A"]
        A_text = "A = {:.0f} m2".format(self.A)
        self.set_labels_params(A_params, A_text)

        U_params = label_params["U"]
        U_text = "U = {:.0f} W/m2°C".format(self.U)
        self.set_labels_params(U_params, U_text)

        hot_air_params = label_params["hot_air"]
        self.set_labels_params(hot_air_params, "Hot Air")

        cold_water_params = label_params["cold_water"]
        self.set_labels_params(cold_water_params, "Cold Water")

        result_params = label_params["result"]
        self.res_text = "Total heat transfer: {:>6.1f} kW\n\nEffectiveness: {:>14.2}"
        self.tbox = self.set_labels_params(result_params, self.res_text\
                    .format(self.q_kw, self.e))

        self.t2in2.set_visible(False)
        self.t2out2.set_visible(False)

    def set_chart(self):
        self.fig = plt.figure(figsize=(9,6))
        self.ax = self.fig.add_subplot(1, 1, 1)
        # HEX configuration plot
        self.ax.axis("off")
        self.hex_config_plot = img.imread(os.path.join(self.png_loc, self.hexpng))
        self.imagebox = OffsetImage(self.hex_config_plot, zoom=0.4)
        ab = AnnotationBbox(self.imagebox, (0.45, 0.7), frameon=False)
        self.ax.add_artist(ab)
        # Widgets
        self.set_sliders()
        self.set_radios()
        self.set_buttons()
        self.set_labels()
