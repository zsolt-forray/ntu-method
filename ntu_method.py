#!/usr/bin/python3


"""
The effectiveness - NTU method calculates the rate of heat transfer
when outlet temperatures are not available.
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '23/11/2019'
__status__  = 'Development'


import numpy as np
from chart_plotting import ChartPlotting


class NTUMethod(ChartPlotting):
    def __init__(self):
        ChartPlotting.__init__(self)
        self.flow_type  = "counter"
        self.T_warm_in  = 120    # °C
        self.m_warm     = 10     # kg/s
        self.c_warm     = 1014   # J/kg°C
        self.T_cold_in  = 16     # °C
        self.m_cold     = 7.5    # kg/s
        self.c_cold     = 4197   # J/kg°C
        self.A          = 40     # m2
        self.U          = 225    # W/m2°C

    def calc_capacity(self):
        # Capacity
        C_warm = self.m_warm * self.c_warm
        C_cold = self.m_cold * self.c_cold
        return C_warm, C_cold

    @staticmethod
    def calc_capacity_ratio(C_warm, C_cold):
        return min(C_warm, C_cold) / max(C_warm, C_cold)

    def calc_NTU(self, C_warm, C_cold):
        # NTU
        return self.U * self.A / min(C_warm, C_cold)

    @staticmethod
    def select_figure(flow_type):
        fig = {
                "parallel": "parallelflow_hex.png",
                "counter": "counterflow_hex.png"
                }
        return fig[flow_type]

    def calc_effectiveness(self, Cr, NTU):
        # Effectiveness
        if self.flow_type == "parallel":
            # Parallel flow
            eff = (1 - np.exp(-NTU * (1 + Cr))) / (1 + Cr)
        elif self.flow_type == "counter":
            # Counter flow
            eff = (1 - np.exp(-NTU * (1 - Cr))) / (1 - Cr * np.exp(-NTU * (1 - Cr)))
        return eff

    def calc_heat_transfer_rate(self, C_warm, C_cold):
        # Heat Transfer Rate
        qmax = min(C_warm, C_cold) * (self.T_warm_in - self.T_cold_in)
        q = self.e * qmax
        return q / 1000

    def calc_outlet_warm_temperature(self, q_kw, C_warm):
        # Outlet warm temperature
        return self.T_warm_in - q_kw * 1000 / C_warm

    def calc_outlet_cold_temperature(self, q_kw, C_cold):
        # Outlet cold temperature
        return q_kw * 1000 / C_cold + self.T_cold_in

    def run_calc(self):
        C_warm, C_cold = self.calc_capacity()
        Cr = NTUMethod.calc_capacity_ratio(C_warm, C_cold)
        NTU = self.calc_NTU(C_warm, C_cold)
        self.e = self.calc_effectiveness(Cr, NTU)
        self.q_kw = self.calc_heat_transfer_rate(C_warm, C_cold)
        self.T_warm_out = self.calc_outlet_warm_temperature(self.q_kw, C_warm)
        self.T_cold_out = self.calc_outlet_cold_temperature(self.q_kw, C_cold)
        self.hexpng = NTUMethod.select_figure(self.flow_type)
        return self.T_warm_out, self.T_cold_out, self.e, self.q_kw


def run_app():
    ntuobj = NTUMethod()
    ntuobj.run_calc()
    ntuobj.run_plot()


if __name__ == "__main__":
    run_app()
